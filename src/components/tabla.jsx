import React from 'react';
export function Tabla({ tabla }) {

    const asignarFecha = (f) => {
        const MESES = [
        "Ene",
        "Feb",
        "Mar",
        "Abr",
        "May",
        "Jun",
        "Jul",
        "Ago",
        "Sept",
        "Oct",
        "Nov",
        "Dic",
        ];
        const aux = f.split('/');
        return aux[0] + ' ' + MESES[parseInt(aux[1])]
    }

    function ordenarRolyMes(a, b) {
        if (a.rol === b.rol) {
            if (parseInt(a["fecha creacion"].split("/")[1]) - parseInt(b["fecha creacion"].split("/")[1]) === 0)
                return parseInt(a["fecha creacion"].split("/")[0]) - parseInt(b["fecha creacion"].split("/")[0])
            return parseInt(a["fecha creacion"].split("/")[1]) - parseInt(b["fecha creacion"].split("/")[1])
        }
        return a.rol > b.rol;
    }
    
    return <div>
        <table>
            <thead>
                <tr>
                    <th>Rut</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>telefono</th>
                    <th>rol</th>
                    <th>empresa</th>
                    <th>fecha</th>
                </tr>
            </thead>
            <tbody>
                {tabla.sort(ordenarRolyMes).map((fila) => (
                    
                    <tr key = {fila.rut}>
                        <td>{ fila.rut }</td>
                        <td>{ fila.nombre }</td>
                        <td>{ fila.correo }</td>
                        <td>{ fila.telefono }</td>
                        <td>{ fila.rol }</td>
                        <td>{ fila.empresa }</td>
                        <td>{
                            
                            asignarFecha(fila["fecha creacion"])
                        
                        }</td>
                    </tr>
                ))}
            </tbody>
      </table>
    </div>;
}
