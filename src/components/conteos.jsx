import React from 'react';

export function Conteos({ tabla }) {
    
    const roles = (t) => {
        const aux = [...new Set(t.map((r) => r.rol))];
        return aux;
    }

    const Roles = roles(tabla);

    const contadorPorMes = (c) => {
        const lista = [];
        for (let i = 1; i <= 12; i++) {
            const filtradosMes = c.filter( mes => {
                return parseInt(mes["fecha creacion"].split("/")[1]) === i;
            });
            Roles.forEach(element => {
                const aux = filtradosMes.filter( r => {
                return r.rol === element;
                }).length;
                lista.push({ rol: element, mes: i, cantidad:aux})
            });
        }
        return lista;
    }

    const asignarMes = (i) => {
        const MESES = [
        "Ene",
        "Feb",
        "Mar",
        "Abr",
        "May",
        "Jun",
        "Jul",
        "Ago",
        "Sept",
        "Oct",
        "Nov",
        "Dic",
        ];
        return MESES[i];
    }
    
    function ordenarContadores (a, b) {
        if (a.rol === b.rol) {
            return a.mes - b.mes
        }
        return a.rol > b.rol;
    }

    return <div>
       <table>
            <thead>
                <tr>
                    <th>Rol</th>
                    <th>Mes</th>
                    <th>Cantidad</th>
                </tr>
            </thead>
            <tbody>
                {
                contadorPorMes(tabla).sort(ordenarContadores).map((fila) => (
                    <tr key= {fila.rol+fila.mes}>
                        <td>{ fila.rol }</td>
                        <td>{asignarMes(fila.mes-1)}</td>
                        <td>{ fila.cantidad }</td>
                    </tr>
                ))
            }
            </tbody>
        </table>
    </div>;
}
