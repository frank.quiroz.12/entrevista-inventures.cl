import './App.css';
import React, { useState, Fragment } from 'react';
import { Tabla } from "./components/tabla";
import { Conteos } from './components/conteos';

export function App() {
  const [tablas] = useState([
  {
    "rut": "13155811-2",
    "nombre": "adelardo linares pina",
    "correo": "adelardo.linares.pina@yahoo.es",
    "telefono": 933662161,
    "rol": "Operador",
    "empresa": "12043514-0 China Construction Bank",
    "fecha creacion": "3/05/2021"
  },
  {
    "rut": "9557194-8",
    "nombre": "agueda boix luz",
    "correo": "agueda.boix.luz@gmail.com",
    "telefono": 918415720,
    "rol": "Operador",
    "empresa": "10882185-k SoftBank Group",
    "fecha creacion": "21/08/2021"
  },
  {
    "rut": "11099974-7",
    "nombre": "aitor valentin angel",
    "correo": "aitor.valentin.angel@yahoo.es",
    "telefono": 910402371,
    "rol": "Encargado",
    "empresa": "24566099-5 Industrial & Commercial Bank of China",
    "fecha creacion": "17/08/2021"
  },
  {
    "rut": "22838951-k",
    "nombre": "amada saenz barranco",
    "correo": "amada.saenz.barranco@yahoo.es",
    "telefono": 956262188,
    "rol": "Ejecutivo",
    "empresa": "10882185-k SoftBank Group",
    "fecha creacion": "6/01/2021"
  },
  {
    "rut": "7094964-4",
    "nombre": "amado lucena solis",
    "correo": "amado.lucena.solis@gmail.com",
    "telefono": 945384440,
    "rol": "Encargado",
    "empresa": "10882185-k SoftBank Group",
    "fecha creacion": "17/03/2021"
  },
  {
    "rut": "14888047-6",
    "nombre": "america brigida barragan",
    "correo": "america.brigida.barragan@hotmail.com",
    "telefono": 918835102,
    "rol": "Encargado",
    "empresa": "14305598-1 Microsoft",
    "fecha creacion": "20/02/2021"
  },
  {
    "rut": "11704098-4",
    "nombre": "aranzazu lucena vizcaino",
    "correo": "aranzazu.lucena.vizcaino@yahoo.es",
    "telefono": 904350077,
    "rol": "Ejecutivo",
    "empresa": "24566099-5 Industrial & Commercial Bank of China",
    "fecha creacion": "9/09/2021"
  },
  {
    "rut": "12342781-5",
    "nombre": "arcelia martinez rossello",
    "correo": "arcelia.martinez.rossello@yahoo.es",
    "telefono": 959167361,
    "rol": "Operador",
    "empresa": "6737982-9 Apple",
    "fecha creacion": "29/04/2021"
  },
  {
    "rut": "8783899-4",
    "nombre": "aristides fortuny pozo",
    "correo": "aristides.fortuny.pozo@hotmail.com",
    "telefono": 929359280,
    "rol": "Encargado",
    "empresa": "6409492-0 Facebook",
    "fecha creacion": "27/06/2021"
  },
  {
    "rut": "15586230-0",
    "nombre": "aurea riquelme gomis",
    "correo": "aurea.riquelme.gomis@yahoo.es",
    "telefono": 971854899,
    "rol": "Gestor",
    "empresa": "6737982-9 Apple",
    "fecha creacion": "1/08/2021"
  },
  {
    "rut": "20854705-4",
    "nombre": "casandra palomares-folch andrade",
    "correo": "casandra.palomares-folch.andrade@yahoo.es",
    "telefono": 912269611,
    "rol": "Gestor",
    "empresa": "6409492-0 Facebook",
    "fecha creacion": "25/06/2021"
  },
  {
    "rut": "23827119-3",
    "nombre": "chus de palma",
    "correo": "chus.de.palma@hotmail.com",
    "telefono": 973860118,
    "rol": "Gestor",
    "empresa": "12039291-3 Saudi Aramco",
    "fecha creacion": "1/07/2021"
  },
  {
    "rut": "20404014-1",
    "nombre": "cleto agusti roman",
    "correo": "cleto.agusti.roman@hotmail.com",
    "telefono": 961317886,
    "rol": "Operador",
    "empresa": "11578774-8 Alphabet",
    "fecha creacion": "20/04/2021"
  },
  {
    "rut": "6843510-2",
    "nombre": "cosme feliu clemente",
    "correo": "cosme.feliu.clemente@yahoo.es",
    "telefono": 935619309,
    "rol": "Operador",
    "empresa": "22724464-k Agricultural Bank of China",
    "fecha creacion": "17/02/2021"
  },
  {
    "rut": "5502325-5",
    "nombre": "daniela torrents alarcon",
    "correo": "daniela.torrents.alarcon@hotmail.com",
    "telefono": 966034177,
    "rol": "Ejecutivo",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "12/04/2021"
  },
  {
    "rut": "17264567-4",
    "nombre": "emiliana corominas grande",
    "correo": "emiliana.corominas.grande@hotmail.com",
    "telefono": 930812962,
    "rol": "Operador",
    "empresa": "12039291-3 Saudi Aramco",
    "fecha creacion": "8/01/2021"
  },
  {
    "rut": "23288618-8",
    "nombre": "faustino martin lumbreras",
    "correo": "faustino.martin.lumbreras@gmail.com",
    "telefono": 946546715,
    "rol": "Operador",
    "empresa": "6409492-0 Facebook",
    "fecha creacion": "2/08/2021"
  },
  {
    "rut": "24814475-0",
    "nombre": "federico cuenca-hernandez palma",
    "correo": "federico.cuenca-hernandez.palma@hotmail.com",
    "telefono": 971435180,
    "rol": "Ejecutivo",
    "empresa": "14305598-1 Microsoft",
    "fecha creacion": "22/04/2021"
  },
  {
    "rut": "22316059-k",
    "nombre": "fito javi bayon",
    "correo": "fito.javi.bayon@yahoo.es",
    "telefono": 997007137,
    "rol": "Gestor",
    "empresa": "6409492-0 Facebook",
    "fecha creacion": "22/10/2021"
  },
  {
    "rut": "24168165-3",
    "nombre": "flavia mateo chacon",
    "correo": "flavia.mateo.chacon@hotmail.com",
    "telefono": 961293529,
    "rol": "Operador",
    "empresa": "12043514-0 China Construction Bank",
    "fecha creacion": "29/03/2021"
  },
  {
    "rut": "19925685-8",
    "nombre": "francisco javier ortuño",
    "correo": "francisco.javier.ortuño@gmail.com",
    "telefono": 999208609,
    "rol": "Gestor",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "27/06/2021"
  },
  {
    "rut": "7649360-k",
    "nombre": "francisco pazos-peña vidal",
    "correo": "francisco.pazos-peña.vidal@yahoo.es",
    "telefono": 935318400,
    "rol": "Gestor",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "8/01/2021"
  },
  {
    "rut": "11937133-3",
    "nombre": "fulgencio peinado cisneros",
    "correo": "fulgencio.peinado.cisneros@yahoo.es",
    "telefono": 906224400,
    "rol": "Operador",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "5/03/2021"
  },
  {
    "rut": "14314886-6",
    "nombre": "hilario esteban miro",
    "correo": "hilario.esteban.miro@hotmail.com",
    "telefono": 973667023,
    "rol": "Operador",
    "empresa": "12043514-0 China Construction Bank",
    "fecha creacion": "17/03/2021"
  },
  {
    "rut": "22007021-2",
    "nombre": "iban villanueva gaya",
    "correo": "iban.villanueva.gaya@gmail.com",
    "telefono": 908309835,
    "rol": "Gestor",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "26/09/2021"
  },
  {
    "rut": "7892094-7",
    "nombre": "isidoro torrijos ayala",
    "correo": "isidoro.torrijos.ayala@yahoo.es",
    "telefono": 923990021,
    "rol": "Operador",
    "empresa": "14305598-1 Microsoft",
    "fecha creacion": "3/08/2021"
  },
  {
    "rut": "7141511-2",
    "nombre": "jacinto blasco calatayud",
    "correo": "jacinto.blasco.calatayud@gmail.com",
    "telefono": 914047806,
    "rol": "Operador",
    "empresa": "12043514-0 China Construction Bank",
    "fecha creacion": "23/04/2021"
  },
  {
    "rut": "21899480-6",
    "nombre": "jafet del izaguirre",
    "correo": "jafet.del.izaguirre@gmail.com",
    "telefono": 991664125,
    "rol": "Operador",
    "empresa": "22724464-k Agricultural Bank of China",
    "fecha creacion": "15/04/2021"
  },
  {
    "rut": "21313021-8",
    "nombre": "jesusa cortes-bejarano solis",
    "correo": "jesusa.cortes-bejarano.solis@hotmail.com",
    "telefono": 919796172,
    "rol": "Operador",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "3/05/2021"
  },
  {
    "rut": "22537935-1",
    "nombre": "juan antonio cortes-vergara",
    "correo": "juan.antonio.cortes-vergara@yahoo.es",
    "telefono": 994018768,
    "rol": "Operador",
    "empresa": "12039291-3 Saudi Aramco",
    "fecha creacion": "5/06/2021"
  },
  {
    "rut": "23509369-3",
    "nombre": "lola camara rossello",
    "correo": "lola.camara.rossello@yahoo.es",
    "telefono": 937507655,
    "rol": "Ejecutivo",
    "empresa": "22724464-k Agricultural Bank of China",
    "fecha creacion": "11/02/2021"
  },
  {
    "rut": "22615548-1",
    "nombre": "marco alfonso rius",
    "correo": "marco.alfonso.rius@yahoo.es",
    "telefono": 928582123,
    "rol": "Operador",
    "empresa": "6409492-0 Facebook",
    "fecha creacion": "25/06/2021"
  },
  {
    "rut": "10425328-8",
    "nombre": "maria del camacho",
    "correo": "maria.del.camacho@hotmail.com",
    "telefono": 981039662,
    "rol": "Gestor",
    "empresa": "10882185-k SoftBank Group",
    "fecha creacion": "5/08/2021"
  },
  {
    "rut": "7297115-9",
    "nombre": "maria pilar alfaro",
    "correo": "maria.pilar.alfaro@yahoo.es",
    "telefono": 999871654,
    "rol": "Operador",
    "empresa": "24566099-5 Industrial & Commercial Bank of China",
    "fecha creacion": "10/02/2021"
  },
  {
    "rut": "10299976-2",
    "nombre": "maricruz escolano vidal",
    "correo": "maricruz.escolano.vidal@hotmail.com",
    "telefono": 973042743,
    "rol": "Operador",
    "empresa": "6409492-0 Facebook",
    "fecha creacion": "16/07/2021"
  },
  {
    "rut": "11401919-4",
    "nombre": "martin de olivera",
    "correo": "martin.de.olivera@gmail.com",
    "telefono": 986328728,
    "rol": "Operador",
    "empresa": "22724464-k Agricultural Bank of China",
    "fecha creacion": "1/05/2021"
  },
  {
    "rut": "6361027-5",
    "nombre": "natalio losada artigas",
    "correo": "natalio.losada.artigas@gmail.com",
    "telefono": 901470156,
    "rol": "Encargado",
    "empresa": "10882185-k SoftBank Group",
    "fecha creacion": "25/01/2021"
  },
  {
    "rut": "19451030-6",
    "nombre": "olivia peñas guerrero",
    "correo": "olivia.peñas.guerrero@gmail.com",
    "telefono": 986863993,
    "rol": "Gestor",
    "empresa": "11578774-8 Alphabet",
    "fecha creacion": "10/06/2021"
  },
  {
    "rut": "7991718-4",
    "nombre": "plinio jove barbera",
    "correo": "plinio.jove.barbera@gmail.com",
    "telefono": 914065787,
    "rol": "Operador",
    "empresa": "24566099-5 Industrial & Commercial Bank of China",
    "fecha creacion": "25/07/2021"
  },
  {
    "rut": "14349818-2",
    "nombre": "ramiro mariño pozo",
    "correo": "ramiro.mariño.pozo@hotmail.com",
    "telefono": 938317294,
    "rol": "Gestor",
    "empresa": "24566099-5 Industrial & Commercial Bank of China",
    "fecha creacion": "22/03/2021"
  },
  {
    "rut": "8668030-0",
    "nombre": "rodrigo asenjo redondo",
    "correo": "rodrigo.asenjo.redondo@gmail.com",
    "telefono": 995087591,
    "rol": "Gestor",
    "empresa": "12039291-3 Saudi Aramco",
    "fecha creacion": "10/05/2021"
  },
  {
    "rut": "8234214-1",
    "nombre": "rogelio español caceres",
    "correo": "rogelio.español.caceres@gmail.com",
    "telefono": 920158346,
    "rol": "Ejecutivo",
    "empresa": "24566099-5 Industrial & Commercial Bank of China",
    "fecha creacion": "10/02/2021"
  },
  {
    "rut": "11515373-0",
    "nombre": "roque oliveras toledo",
    "correo": "roque.oliveras.toledo@hotmail.com",
    "telefono": 900299120,
    "rol": "Operador",
    "empresa": "14305598-1 Microsoft",
    "fecha creacion": "26/10/2021"
  },
  {
    "rut": "9501888-2",
    "nombre": "ruth jordan pacheco",
    "correo": "ruth.jordan.pacheco@hotmail.com",
    "telefono": 963589283,
    "rol": "Ejecutivo",
    "empresa": "6737982-9 Apple",
    "fecha creacion": "22/10/2021"
  },
  {
    "rut": "11546531-7",
    "nombre": "salomon bernardo cobos",
    "correo": "salomon.bernardo.cobos@yahoo.es",
    "telefono": 940332347,
    "rol": "Operador",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "9/05/2021"
  },
  {
    "rut": "18863125-8",
    "nombre": "samuel serna jurado",
    "correo": "samuel.serna.jurado@gmail.com",
    "telefono": 975465988,
    "rol": "Operador",
    "empresa": "23690053-3 Berkshire Hathaway",
    "fecha creacion": "29/04/2021"
  },
  {
    "rut": "19545422-1",
    "nombre": "severo murcia llorente",
    "correo": "severo.murcia.llorente@yahoo.es",
    "telefono": 904228262,
    "rol": "Ejecutivo",
    "empresa": "10882185-k SoftBank Group",
    "fecha creacion": "11/09/2021"
  },
  {
    "rut": "13656607-5",
    "nombre": "tomasa criado castell",
    "correo": "tomasa.criado.castell@hotmail.com",
    "telefono": 972984726,
    "rol": "Gestor",
    "empresa": "12039291-3 Saudi Aramco",
    "fecha creacion": "23/02/2021"
  },
  {
    "rut": "7347506-6",
    "nombre": "viviana donaire busquets",
    "correo": "viviana.donaire.busquets@gmail.com",
    "telefono": 955799228,
    "rol": "Encargado",
    "empresa": "12043514-0 China Construction Bank",
    "fecha creacion": "18/10/2021"
  },
  {
    "rut": "11901999-0",
    "nombre": "xiomara de coello",
    "correo": "xiomara.de.coello@hotmail.com",
    "telefono": 969996442,
    "rol": "Ejecutivo",
    "empresa": "6737982-9 Apple",
    "fecha creacion": "12/09/2021"
  }
  ]);

  return (
    <Fragment>
      <Conteos tabla={ tablas } />
      <Tabla tabla={ tablas }/>
    </Fragment>
  )

}

 
